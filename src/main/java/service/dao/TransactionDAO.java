package service.dao;

import org.springframework.stereotype.Repository;
import service.model.Transaction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by piskunov on 11/07/16.
 */

@Repository
public class TransactionDAO {

    private Map<Long, Transaction> transactions = new HashMap<>();

    //create or update a transaction
    public void updateTransaction(Transaction newTransaction) {
            transactions.put(newTransaction.getId(), newTransaction);
    }

    public Transaction findByID(Long id) {
        return transactions.get(id);
    }

    //for testing purposes only since where is no method to remove transactions
    public void cleanup(){
        transactions = new HashMap<>();
    }

}
