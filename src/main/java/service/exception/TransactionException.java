package service.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by piskunov on 12/07/16.
 */
public class TransactionException extends Exception {
    public final static String NO_SUCH_PARENT = "Parent ID not found.";
    public final static String PARENT_ID_LEAD_TO_CYCLE = "Unable to set parent_id: Specified parent_id lead to cyclic structure.";
    public final static String EMPTY_AMOUNT = "Amount is not specified.";
    public final static String EMPTY_TYPE = "Type is not specified.";
    public final static String INVALID_ID = "Transaction ID must be a number.";
    public final static String ID_NOT_FOUND = "Transaction is not found. ID: ";

    HttpStatus status;

    public HttpStatus getStatus() {
        return status;
    }

    public TransactionException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }
}
