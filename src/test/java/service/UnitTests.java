package service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.exception.TransactionException;
import service.logic.TransactionLogic;
import service.model.Transaction;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class UnitTests {

	/*
	Use Cases list:
	1)Create transaction
	2)Update transaction
	3)Get transaction by ID
	4)Get transactions by type
	5)Get summary for a transaction tree

	Test Cases list:
	*1.1)Test create transaction with correct data
	*1.2)Test create transaction with incorrect data (null type or amount)
	*2.1)Update type, amount
	*2.2)Update parent_id
	*2.3)Update parent_id to not existent transaction id
	*2.4)Update parent_id to null
	*2.5)Update parent_id to make cycle structure
	*3.1)Get transaction by id
	*3.2)Get not existent transaction
	*4.1)Get transactions by type
	*4.2)Get transactions by not existent type
	*5.1)Get sum by id
	*5.2)Get sum by not existent id
	*/

	@Autowired
	ApplicationContext context;

	@Autowired
	TransactionLogic transactionLogic;

	//1.1 3.1
	@Test
	public void createTransaction() throws TransactionException {
		transactionLogic.cleanup();
		Transaction t1 = new Transaction(1L, null, 1D, "t1");
		transactionLogic.updateTransaction(t1);
		assert transactionLogic.getTransaction(1L).equals(t1);
	}

	//3.2
	@Test
	public void getNotExistentTransactionById() throws TransactionException {
		transactionLogic.cleanup();
		assert transactionLogic.getTransaction(1L) == null;
	}

	//1.2
	@Test(expected = TransactionException.class)
	public void createTransactionNullAmount() throws TransactionException {
		transactionLogic.cleanup();
		transactionLogic.updateTransaction(new Transaction(1L, null, null, "t1"));
	}
	//1.2
	@Test(expected = TransactionException.class)
	public void createTransactionNullType() throws TransactionException {
		transactionLogic.cleanup();
		transactionLogic.updateTransaction(new Transaction(1L, null, 1D, null));
	}

	//4.1, 4.2
	@Test
	public void getTransactionsByType() throws TransactionException {
		transactionLogic.cleanup();
		prepareTestData(1L);
		assert transactionLogic.getTransactionIdsByType("t1").length == 3;
		assert transactionLogic.getTransactionIdsByType("t2").length == 1;
		assert transactionLogic.getTransactionIdsByType("t5").length == 0;
	}

	//5.1
	@Test
	public void getTransactionsAmountByParent() throws TransactionException {
		transactionLogic.cleanup();
		prepareTestData(1L);
		assert transactionLogic.getTransactionsAmountByParent(1L) == 61D;
		assert transactionLogic.getTransactionsAmountByParent(2L) == 60D;
		assert transactionLogic.getTransactionsAmountByParent(3L) == 20D;
	}

	//5.2
	@Test(expected = TransactionException.class)
	public void getTransactionsAmountByNotExistentParent() throws TransactionException {
		transactionLogic.cleanup();
		prepareTestData(1L);
		transactionLogic.getTransactionsAmountByParent(100L);
	}

	//2.5
	//test cycle structures
	@Test(expected = TransactionException.class)
	public void testCycleStructure() throws TransactionException{
		transactionLogic.cleanup();
		prepareTestData(1L);
		transactionLogic.updateTransaction(new Transaction(1L, 4L, 1D, "t1"));
	}

	//2.1 2.2 4.1 5.1
	@Test
	public void updateParentTypeAmount() throws TransactionException{
		transactionLogic.cleanup();
		prepareTestData(1L);
		prepareTestData(10L);
		transactionLogic.updateTransaction(new Transaction(2L, 10L, 100D, "t3"));
		assert transactionLogic.getTransactionsAmountByParent(10L) == 211D;
		assert transactionLogic.getTransactionIdsByType("t3").length == 1;
	}

	//2.4
	@Test
	public void updateParentToNull() throws TransactionException{
		transactionLogic.cleanup();
		prepareTestData(1L);
		transactionLogic.updateTransaction(new Transaction(2L, null, 100D, "t3"));
		assert transactionLogic.getTransactionsAmountByParent(1L) == 1D;
	}

	//2.3
	@Test(expected = TransactionException.class)
	public void updateParentToNotExistentID() throws TransactionException{
		transactionLogic.cleanup();
		prepareTestData(1L);
		transactionLogic.updateTransaction(new Transaction(2L, 20L, 100D, "t3"));
	}


	private void prepareTestData(Long id) throws TransactionException {
		transactionLogic.updateTransaction(new Transaction(id, null, 1D, "t1"));
		transactionLogic.updateTransaction(new Transaction(id+1, id, 10D, "t1"));
		transactionLogic.updateTransaction(new Transaction(id+2, id+1, 20D, "t1"));
		transactionLogic.updateTransaction(new Transaction(id+3, id+1, 30D, "t2"));
	}
}
