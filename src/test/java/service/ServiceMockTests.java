package service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import service.exception.TransactionException;
import service.logic.TransactionLogic;
import service.model.Transaction;
import service.web.ServiceController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ServiceMockTests {

	/*
	Use Cases list:
	1)Create transaction
	2)Update transaction
	3)Get transaction by ID
	4)Get transactions by type
	5)Get summary for a transaction tree
	*/

	private MockMvc mockMvc;

	@Autowired
	ServiceController controller;

	@Autowired
	TransactionLogic transactionLogic;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	//1-3 cases
	@Test
	public void createAndUpdateSceanrio() throws Exception {
		transactionLogic.cleanup();
		Transaction t1 = new Transaction(1L, null, 1D, "t1");
		this.mockMvc.perform(put("/transactionservice/transaction/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(t1.toString()))
				.andExpect(status().isOk());
		t1.setAmount(null);
		this.mockMvc.perform(put("/transactionservice/transaction/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(t1.toString()))
				.andExpect(status().isBadRequest());
		t1.setAmount(1D);
		t1.setType(null);
		this.mockMvc.perform(put("/transactionservice/transaction/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(t1.toString()))
				.andExpect(status().isBadRequest());
		t1.setType("t1");
		t1.setParentId(2L);
		this.mockMvc.perform(put("/transactionservice/transaction/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(t1.toString()))
				.andExpect(status().isBadRequest());
		t1.setParentId(null);
		t1.setAmount(10D);
		this.mockMvc.perform(put("/transactionservice/transaction/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(t1.toString()))
				.andExpect(status().isOk());
		assert this.mockMvc.perform(get("/transactionservice/transaction/1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString().equals(t1.toString());
		this.mockMvc.perform(get("/transactionservice/transaction/2")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	//4th, 5th case
	@Test
	public void getTransactionsByTypeAndSummary() throws Exception {

		//create test data
		transactionLogic.cleanup();
		transactionLogic.updateTransaction(new Transaction(1L, null, 1D, "t1"));
		transactionLogic.updateTransaction(new Transaction(2L, 1L, 10D, "t1"));
		transactionLogic.updateTransaction(new Transaction(3L, 2L, 20D, "t1"));
		transactionLogic.updateTransaction(new Transaction(4L, 2L, 30D, "t2"));

		assert this.mockMvc.perform(get("/transactionservice/sum/2")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString().equals("{\"sum\":60.0}");

		assert this.mockMvc.perform(get("/transactionservice/types/t1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString().equals("[1,2,3]");
	}



}
