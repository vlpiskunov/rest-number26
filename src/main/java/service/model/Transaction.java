package service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by piskunov on 11/07/16.
 */
public class Transaction {

    @JsonIgnore
    Long id;

    @JsonProperty("parent_id")
    Long parentId;
    Double amount;
    String type;

    public Transaction() {
    }

    public Transaction(Long id, Long parentId, Double amount, String type) {
        this.id = id;
        this.parentId = parentId;
        this.amount = amount;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString(){
        return "{\"amount\":" + amount + ",\"type\":"
                + (type == null ? "null": "\"" + type + "\"")
                + ",\"parent_id\":" + parentId + "}";
    }
}
