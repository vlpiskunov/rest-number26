package service.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import service.exception.TransactionException;
import service.dao.TransactionDAO;
import service.model.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by piskunov on 12/07/16.
 */

@Component
public class TransactionLogic {

    @Autowired
    TransactionDAO transactionDAO;

    //children by parent mapping
    private Map<Long, List<Long>> transactionsByParent = new HashMap<>();
    //transactions by type mapping
    private Map<String, List<Long>> transactionsByType = new HashMap<>();

    //create or update a transaction
    public synchronized void updateTransaction(Transaction newTransaction) throws TransactionException {
        Long id = newTransaction.getId();
        String oldType = null;
        String newType = newTransaction.getType();
        Long oldParentId = null;
        Long newParentId = newTransaction.getParentId();
        Transaction oldTransaction = transactionDAO.findByID(id);
        if (oldTransaction != null) {
            oldType = oldTransaction.getType();
            oldParentId = oldTransaction.getParentId();
        }

        validateData(newTransaction, oldParentId);

        //update parent-child mapping
        updateMapping(transactionsByParent, oldParentId, newParentId, id);
        //update type mapping
        updateMapping(transactionsByType, oldType, newType, id);
        transactionDAO.updateTransaction(newTransaction);
    }

    public Transaction getTransaction(Long id) {
        return transactionDAO.findByID(id);
    }

    public Long[] getTransactionIdsByType(String type) {
        List<Long> ids = transactionsByType.get(type);
        return ids == null ? new Long[0] : ids.toArray(new Long[ids.size()]);
    }

    public Double getTransactionsAmountByParent(Long parentId) throws TransactionException {
        if(transactionDAO.findByID(parentId) == null) {
            throw new TransactionException(TransactionException.ID_NOT_FOUND + parentId, HttpStatus.NOT_FOUND);
        }

        Double sum = 0d;
        List<Long> transactionIds = getTransactionTreeAsList(parentId);
        transactionIds.add(parentId);
        for (Long id : transactionIds) {
            sum += transactionDAO.findByID(id).getAmount();
        }
        return sum;
    }

    //for testing purposes only since where is no method to remove transactions
    public void cleanup() {
        transactionsByParent = new HashMap<>();
        transactionsByType = new HashMap<>();
        transactionDAO.cleanup();
    }

    private List<Long> getTransactionTreeAsList(Long parentId) {
        List<Long> transactionIDs = new ArrayList<>();
        List<Long> children = transactionsByParent.get(parentId);
        if (children != null) {
            transactionIDs.addAll(children);
            for (Long childId : children) {
                transactionIDs.addAll(getTransactionTreeAsList(childId));
            }
        }
        return transactionIDs;
    }

    private void validateData(Transaction transaction, Long oldParentID) throws TransactionException {
        if (transaction.getType() == null) {
            throw new TransactionException(TransactionException.EMPTY_TYPE, HttpStatus.BAD_REQUEST);
        }
        if (transaction.getAmount() == null) {
            throw new TransactionException(TransactionException.EMPTY_AMOUNT, HttpStatus.BAD_REQUEST);
        }

        Long parentId = transaction.getParentId();
        if (parentId != null) {
            if (transactionDAO.findByID(parentId) == null) {
                throw new TransactionException(TransactionException.NO_SUCH_PARENT, HttpStatus.BAD_REQUEST);
            }
            if (!parentId.equals(oldParentID) && getTransactionTreeAsList(transaction.getId()).contains(parentId)) {
                throw new TransactionException(TransactionException.PARENT_ID_LEAD_TO_CYCLE, HttpStatus.BAD_REQUEST);
            }
        }
    }

    private <K, V> void updateMapping(Map<K, List<V>> map, K oldKey, K newKey, V value) {
        if(newKey == oldKey) {
            return;
        }
        if (oldKey != null) {
            map.get(oldKey).remove(value);
        }
        if (newKey != null) {
            List<V> values = map.get(newKey);
            if (values != null) {
                values.add(value);
            } else {
                values = new ArrayList<V>();
                values.add(value);
                map.put(newKey, values);
            }
        }
    }
}
