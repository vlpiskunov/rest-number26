package service.web;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by piskunov on 13/07/16.
 */
public class ServiceResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    Double sum;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    String status;

    public ServiceResponse(String message, String status) {
        this.message = message;
        this.status = status;
    }

    public ServiceResponse(Double sum) {
        this.sum = sum;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }
}
