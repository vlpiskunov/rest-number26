package service.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import service.exception.TransactionException;
import service.logic.TransactionLogic;
import service.model.Transaction;

/**
 * Created by piskunov on 11/07/16.
 */


@RestController
@RequestMapping("/transactionservice")
public class ServiceController {

    @Autowired
    TransactionLogic transactionLogic;

    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ServiceResponse>
    updateTransaction(@PathVariable Long id,
                      @RequestBody Transaction transaction) throws TransactionException {
        transaction.setId(id);
        transactionLogic.updateTransaction(transaction);
        return new ResponseEntity<>(new ServiceResponse(null, HttpStatus.OK.name()), HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.GET)
    public ResponseEntity<Transaction> getTransaction(@PathVariable Long id) throws TransactionException {
        Transaction transaction = transactionLogic.getTransaction(id);
        if (transaction != null) {
            return new ResponseEntity<>(transaction, HttpStatus.OK);
        }
        throw new TransactionException(TransactionException.ID_NOT_FOUND + id, HttpStatus.NOT_FOUND);

    }

    @RequestMapping(value = "/types/{type}", method = RequestMethod.GET)
    public ResponseEntity<Long[]> getTransactionsByType(@PathVariable String type) {
        return new ResponseEntity<>(transactionLogic.getTransactionIdsByType(type), HttpStatus.OK);
    }

    @RequestMapping(value = "/sum/{transactionId}", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getTransactionSummary(@PathVariable Long transactionId)
            throws TransactionException {
        Double sum = transactionLogic.getTransactionsAmountByParent(transactionId);
        return new ResponseEntity<>(new ServiceResponse(sum), HttpStatus.OK);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ServiceResponse> exceptionHandler(Exception exception) {
        HttpStatus status = null;
        String message = null;

        if (exception instanceof TransactionException) {
            status = ((TransactionException) exception).getStatus();
            message = exception.getMessage();
        } else if (exception instanceof MethodArgumentTypeMismatchException &&
                exception.getCause() instanceof NumberFormatException) {
            status = HttpStatus.BAD_REQUEST;
            message = TransactionException.INVALID_ID;
        } else {
            exception.printStackTrace();
            message = exception.getClass().getName() + ": " + exception.getMessage();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        ServiceResponse serviceResponse = new ServiceResponse(message, status.name());
        return new ResponseEntity<>(serviceResponse, status);
    }
}

